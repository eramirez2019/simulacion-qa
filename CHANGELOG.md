Control de Cambios Simulacion QA
=======================================

[1.0.0 2019-24-09 https://tecnocen.atlassian.net/browse/VT-1170]

- [Enh] `backend/views/pruebas`
   Se añaden cambios en página (MTellez)

- [Enh] `backend/views/pruebas/pagina`
   Se añaden correcciones y cambios ortograficos (MTellez)
