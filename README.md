Página de simulación QA

Introducción
Esta página tiene el propósito de ayudar a los nuevos integrantes del Equipo de QA a tener no solo conocimientos teóricos sino que también prácticos para próximas pruebas ya sea de Android y iOS.
A su vez también ayuda a tener un poco más de entendimiento de los procesos que se tienen que hacer dentro del área.


Pasos a seguir para la página web
Contar con una laptop o computadora de escritorio.
Sistema operativo iOS, Windows o Ubuntu
Contar con un Navegador Web ya sea Chrome, FireFox, Safari u Opera.
